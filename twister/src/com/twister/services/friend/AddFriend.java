package com.twister.services.friend;

import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;

import com.twister.servicesTools.ServiceRefused;
import com.twister.tools.FriendTool;
import com.twister.tools.LoginTool;
import com.twister.tools.UserTool;

public class AddFriend {
	
	public static JSONObject addFriend(String key, String friend_id) throws JSONException, SQLException {
		JSONObject ret = new JSONObject();
		
		if(!LoginTool.keyExists(key)) {
			ret.put("Status", "KO");
			ret.put("error", "user doesn't exists 1"); 
			return ret;
		}
		
		if(!UserTool.userExistsId(friend_id)) {
			System.out.println("f"+friend_id);
			ret.put("Status", "KO");
			ret.put("error", "user doesn't exists 2"); 
			return ret;
		}
		
		boolean ajout = FriendTool.addFriend(key,friend_id);
		if(!ajout) {
			ret.put("Status", "KO");
			ret.put("error", "deja ami"); 
			System.out.println("Ajout KO");
			return ret;
		}else {
		ret.put("Status","OK");
		ret.put("Msg","Votre ami a ete ajout� !");
		System.out.println("Ajout OK");
		return ret;
		}
		
		
	}

}

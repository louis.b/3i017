package com.twister.services.friend;

import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;

import com.twister.tools.FriendTool;
import com.twister.tools.LoginTool;
import com.twister.tools.UserTool;

public class GetFriends {

	public static JSONObject getFriends(String key,String id) throws JSONException, SQLException {
		// TODO Auto-generated method stub
		JSONObject ret = new JSONObject();
		
		if(!LoginTool.keyExists(key)) {
			ret.put("Status", "KO");
			ret.put("error", "no key exists");
			return ret;
		}
		if(!UserTool.userExistsId(id)) {
			ret.put("Status", "KO");
			ret.put("error", "user doesn't exists");
			return ret;
		}		ret.put("Status","OK");
		ret.put("friends",FriendTool.getFriends(id));
		return ret;
	}

}

package com.twister.services.friend;

import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;

import com.twister.bd.FriendBD;
import com.twister.tools.FriendTool;
import com.twister.tools.LoginTool;
import com.twister.tools.UserTool;

public class RemoveFriend {
	public static JSONObject removeFriend(String key, String id_friend) throws JSONException, SQLException  {
		
		JSONObject ret = new JSONObject();
		System.out.println("id_friend "+id_friend);
		if(!LoginTool.keyExists(key)) {
			ret.put("Status", "KO");
			ret.put("error", "no key exists");
			return ret;
		}
		
		if(!UserTool.userExistsId(id_friend)) {
			ret.put("Status", "KO");
			ret.put("error", "user doesn't exists");
			return ret;
		}
		
		FriendTool.removeFriend(key,id_friend);
		ret.put("Status","OK");
		return ret;
	}
	
}

package com.twister.services;

public class Operation_Service {
	private static double addition(double a, double b) {
		return a+b;	
	}
	public static double multiplication(double a, double b) {
		return a*b;	
	}
	public static double division(double a, double b) {
		return a/b;	
	}
	public static double calcul(double a, double b,String operation) {

			switch (operation) {
				case "plus": return addition(a,b);
				case "div": return division(a,b);
				case "mult": return multiplication(a,b);
				default : return addition(a,b);
			}

	}
}

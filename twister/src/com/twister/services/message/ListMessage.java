package com.twister.services.message;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.twister.bd.FriendBD;
import com.twister.bd.MessageBD;
import com.twister.tools.LoginTool;
import com.twister.tools.MessageTool;
import com.twister.tools.UserTool;

public class ListMessage {

	public static JSONObject listMessage(String key,String query,String fromId) throws JSONException, SQLException {
		JSONObject ret = new JSONObject();
		String my_id;
		int[] auteurs;
		if(!LoginTool.keyExists(key)) {
			ret.put("Status", "KO");
			ret.put("error", "no key exists");
			return ret;
		}
		
		if((my_id = LoginTool.getId(key)) == null) {
			ret.put("Status", "KO");
			ret.put("error", "List message - Id dont exists");
			return	ret;
		}
		if(fromId != null) {
			if(!UserTool.userExistsId(fromId)) {
				ret.put("Status", "KO");
				ret.put("error", "addComment - comment is null");
				return	ret;
			}
			my_id = fromId;
		}
		auteurs=FriendBD.getFriendsMeId(my_id);
		System.out.println(auteurs[0]);
		JSONObject msgs;
		int nb_msgs = 0;
		if(query!=null) {
			if(query.equals("all")) {
				ret = MessageTool.getAllMessages();				
			}
			else {
				auteurs= new int[] {};// traitement de query
				ret = MessageTool.getMessagesByIds(auteurs);
			}
		}
		else {
			msgs = MessageTool.getMessagesByIds(auteurs);
			if(msgs != null) {
				ret=msgs;
				ret.getJSONArray("messages").put(MessageBD.getMessagesId(my_id));
				//ret.getJSONArray("messages");
			}
		}
		nb_msgs = ret.getJSONArray("messages").length();
		ret.put("Status", "OK");
		ret.put("nb", nb_msgs);
		return ret;
	}
}

package com.twister.services.message;

import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;

import com.twister.tools.LoginTool;
import com.twister.tools.MessageTool;

public class RemoveMessage {

	public static JSONObject removeMessage(String key,String comment) throws JSONException, SQLException {
		JSONObject ret = new JSONObject();
		if(!LoginTool.keyExists(key)) {
//			ret=ServiceRefused.serviceRefused("Status", 500);
			ret.put("Status", "KO");
			ret.put("error", "no key exists");
			return ret;
		}
		
		if(comment ==null) {
			ret.put("Status", "KO");
			ret.put("error", "RemoveMessage - message is null");
			return ret;			
		}

		if(!MessageTool.messageExists(comment)) {
			ret.put("Status", "KO");
			ret.put("error", "RemoveMessage - message do not exists");
			return ret;			
		}
		MessageTool.deleteMessage(comment);
		ret.put("Status", "OK");
		ret.put("Message", "message supprim�");
		return ret;
	}

}

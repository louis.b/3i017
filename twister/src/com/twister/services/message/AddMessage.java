package com.twister.services.message;

import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;

import com.twister.tools.LoginTool;
import com.twister.tools.MessageTool;
import com.twister.tools.UserTool;

public class AddMessage {
	
	public static JSONObject addMessage(String key, String message) throws JSONException, SQLException {
		
		JSONObject ret = new JSONObject();
		if(!LoginTool.keyExists(key)) {
			ret.put("Status", "KO");
			ret.put("error", "no key exists");
			return ret;
		}
		
		if(message == null || message.length() == 0) {
			ret.put("Status", "KO");
			ret.put("error", "addComment - message is null");
			return ret;			
		}
//		if(!MessageTool.messageExists(message)) {
//			ret.put("Status", "KO");
//			ret.put("error", "RemoveMessage - message do not exists");
//			return ret;			
//		}
		String id = LoginTool.getId(key);
		MessageTool.addMessagetoBD(Integer.parseInt(id),UserTool.getLoginByID(id),message);
		ret.put("Status", "OK");
		//QQCH AVEC KEY
		ret.put("Message", message);
		return ret;
	}
	public static JSONObject addComment(String key, String message,String message_id) throws JSONException, SQLException {
		
		JSONObject ret = new JSONObject();
		if(!LoginTool.keyExists(key)) {
			ret.put("Status", "KO");
			ret.put("error", "no key exists");
			return ret;
		}
		
		if(message == null || message.length() == 0) {
			ret.put("Status", "KO");
			ret.put("error", "addComment - message is null");
			return ret;			
		}
		if(MessageTool.messageExists(message_id)) {
			ret.put("Status", "KO");
			ret.put("error", "addComment - message is null");
			return ret;			
		}
		String id = LoginTool.getId(key);
		MessageTool.addCommenttoBD(Integer.parseInt(id),UserTool.getLoginByID(id),message,message_id);
		ret.put("Status", "OK");
		ret.put("Message", message);
		return ret;
	}
}

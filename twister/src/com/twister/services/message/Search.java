package com.twister.services.message;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.twister.tools.LoginTool;
import com.twister.tools.MessageTool;
import com.twister.tools.UserTool;

public class Search {
	public static JSONObject search(String key, String query) throws JSONException, SQLException {
		JSONObject ret = new JSONObject();
		System.out.println(query);
		if(!LoginTool.keyExists(key) ) {
			ret.put("Status", "KO");
			System.out.println("key out");
			ret.put("error", "key unknown"); 
			return ret;
		}
		if( query ==null|| query.length() == 0) {
			ret = MessageTool.getAllMessages();
			ret.put("Status", "Ok");
			return ret;
			}
		
		HashMap<String,String[]> query_list=  new HashMap<>();
		String[] tmp1 = query.split(";");
		String[] tmp2;
		for(String s : tmp1) {
			tmp2 = s.split(":");
			if(tmp2.length<2) {
				ret.put("messages", new JSONArray());
				ret.put("Status", "Ok");
				return ret;
				}
			query_list.put(tmp2[0].trim(),tmp2[1].split(","));
		}
		String[] user = query_list.get("user");
		String[] cont = query_list.get("cont");
		List<Integer> userbd = null;
		Integer[] list_userbd = {};
		if(user != null) {			
			int id_user ;
			userbd = new ArrayList<>();
			for(int i = 0; i < user.length ; i++ ) {
				if(UserTool.userExists(user[i].trim())) {
					id_user = Integer.parseInt(UserTool.getIdbyLogin(user[i].trim()));				
					userbd.add(id_user);
				}
			}
			if(userbd == null||userbd.size() == 0 ) {
				ret.put("messages", new JSONArray());
				ret.put("Status", "Ok");
				return ret;
			}
		list_userbd = userbd.toArray(new Integer[userbd.size()]);
		}
		String content = null;
		System.out.println(cont);
		if(cont != null) {
			StringBuilder contB = new StringBuilder();
			for(String c : cont) {
				if(c.length()<1) {
					ret.put("Status", "KO");
					System.out.println("Query empty");
					ret.put("error", "Query empty"); 
					return ret;
					}
				content = c.trim();
				contB.append(content.substring(1, content.length()-1)+'|');
			}
			contB.deleteCharAt(contB.length()-1);
			content = contB.toString();
		}
		int[] list_int_userbd = new int[list_userbd.length];
		for(int i = 0; i<list_userbd.length;i++) list_int_userbd[i] = list_userbd[i];
		System.out.println(content);
		System.out.println(userbd);
		if(userbd != null && content == null) {
			ret = MessageTool.getMessagesByIds(list_int_userbd);
			System.out.println("1er");
			ret.put("Status", "Ok");

		}
		else if(userbd != null && content != null) {
			ret = MessageTool.getMessagesByIdsQuery(list_int_userbd,content);
			System.out.println("2eme");

		}

		else if(userbd == null && content != null) {
			ret = MessageTool.getAllMessagesQuery(content);
			System.out.println(ret);
			System.out.println("3eme");

		}
		else {
			ret = MessageTool.getAllMessages();
			System.out.println("4eme");

		}
		if(ret == null) {
			ret = new JSONObject(); ret.put("messages", new JSONArray());
			ret.put("Status", "Ok");

		}
		ret.put("Status", "Ok");
		return ret;
	}

}

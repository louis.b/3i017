package com.twister.services.user;

import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;

import com.twister.tools.LoginTool;

public class RefreshKey {
	public static JSONObject refreshkey(String key) throws JSONException, SQLException {
		JSONObject ret = new JSONObject();
		if(!LoginTool.keyExists(key)) { 
			ret.put("Status", "KO");
			ret.put("error", "user doesn't exists"); 
			return ret;
		}
		else if(LoginTool.isExpired(key)) { 
			ret.put("Status", "KO");
			ret.put("error", "key expired"); 
			return ret;
		}
		LoginTool.refresh(key);
		ret.put("Status", "OK") ;
		return ret;
}
}

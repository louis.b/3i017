package com.twister.services.user;

import org.json.JSONException;
import org.json.JSONObject;

import com.twister.bd.UserBD;
import com.twister.servicesTools.*;
import com.twister.tools.LoginTool;
import com.twister.tools.UserTool;

public class CreateUser {

	public static JSONObject createUser(String login, String prenom, String nom, String pass) throws Exception {
		// TODO Auto-generated method stub
		JSONObject ret = new JSONObject();
		if(UserTool.userExists(login)) {
			ret.put("Status", "KO");
			ret.put("error", "user exists"); 
			return ret;
		}
		else if(pass==null || login==null  || nom==null || prenom==null || pass.length() ==0 || login.length() ==0  || nom.length() ==0 || prenom.length() ==0){
			ret.put("Status", "KO");
			ret.put("error", "missing parameters"); 
			return ret;
		}
		else {
			UserTool.addtoBDUser(login, pass, prenom, nom);
			ret.put("Status", "OK");
			String id = UserTool.getIdbyLogin(login);
			ret.put("id",id);
			boolean root = UserTool.isRoot(login);
			ret.put("key", LoginTool.login(id, root));
			ret.put("login", login);
			return ret;
		}
	}
	// faire avec bd(UserExists ...) et ServiceRefused qui renvoi une erreur
}

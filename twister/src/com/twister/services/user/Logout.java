package com.twister.services.user;

import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;

import com.twister.servicesTools.ServiceRefused;
import com.twister.tools.LoginTool;

public class Logout {

	public static JSONObject logout(String key) throws JSONException, SQLException {
			JSONObject ret = new JSONObject();
			if(!LoginTool.keyExists(key)) { 
				ret.put("Status", "KO");
				ret.put("error", "user doesn't exists"); 
				return ret;
			}

			LoginTool.disconnect(key);
			ret.put("Status", "OK") ;
			return ret;
	}

}

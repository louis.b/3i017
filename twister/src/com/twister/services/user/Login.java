package com.twister.services.user;

import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;

import com.twister.bd.LoginBD;
import com.twister.servicesTools.ServiceRefused;
import com.twister.tools.LoginTool;
import com.twister.tools.UserTool;

public class Login {
	public static JSONObject login(String login , String pass) throws JSONException {
		JSONObject ret = new JSONObject();
		if(!UserTool.userExists(login)) {
			ret.put("Status", "KO");
			ret.put("error", "User doesn't exists"); 
			return ret;
		}
		if(!UserTool.checkPasswd(login,pass)) {
			ret.put("Status", "KO");
			ret.put("error", "Password's wrong"); 
			return ret; 
		}
		String id_user = UserTool.getIdbyLogin(login);
		LoginTool.logoutId(id_user);
		boolean root = UserTool.isRoot(login);
		String key = LoginTool.login(id_user, root); //insererConnexion ?
		ret.put("Status", "OK");
		ret.put("key", key);
		ret.put("login", login);
		ret.put("id",id_user);
		return ret;
	}

	public static JSONObject getlogin(String id, String key) throws JSONException, SQLException {
		JSONObject ret = new JSONObject();
		if(!LoginTool.keyExists(key)) { 
			ret.put("Status", "KO");
			ret.put("error", "key doesn't exists"); 
			return ret;
		}
		if(!UserTool.userExistsId(id)) { 
			ret.put("Status", "KO");
			ret.put("error", "user doesn't exists"); 
			return ret;
		}
		ret.put("login",UserTool.getLoginByID(id));
		ret.put("Status", "OK");
		return ret;
	}
}

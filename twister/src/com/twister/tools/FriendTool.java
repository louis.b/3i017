package com.twister.tools;

import java.util.Collection;

import com.twister.bd.FriendBD;

public class FriendTool {

	public static boolean removeFriend(String key, String id_friend) {
		// TODO Auto-generated method stub
		return FriendBD.removeFriend(key, id_friend);

	}

	public static boolean addFriend(String key, String id_friend) {
		return FriendBD.addFriend(key, id_friend);
		
	}

	public static int[] getFriends(String id) {
		// TODO Auto-generated method stub
		return FriendBD.getFriends(id);
	}

}

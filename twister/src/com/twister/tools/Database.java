package com.twister.tools;

import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.activation.DataSource;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.mongodb.*;
import com.twister.bd.DBStatic;

public class Database {
	private DataSource dataSource;
	public Database(String jndiname) throws SQLException{
		try {
			dataSource = (DataSource) new InitialContext().lookup("java:comp/env/"+jndiname);
		}catch(NamingException e) {
			// handle 
			throw new SQLException(jndiname + "is missing in JNDI! : "+e.getMessage());
		}
	}
	public Connection getConnection() throws SQLException{
		return ((Database) dataSource).getConnection();
	}
	
	public static Connection getMySQLConnection() throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		if(DBStatic.mysql_pooling==false) {
			return(DriverManager.getConnection("jdbc:mysql://"+DBStatic.mysql_host+"/"+DBStatic.mysql_db,DBStatic.mysql_username,DBStatic.mysql_password));
		}
		else {
//			if (database==null) { 
//					database=new Database("jdbc/db");
//					}
			return((new Database("jdbc/db")).getConnection());
			
		}
	}
	public static DBCollection getCollection(String collection) throws UnknownHostException, MongoException {
		return (new Mongo(DBStatic.mongo_url)).getDB(DBStatic.mongo_db).getCollection(collection);
	}
}

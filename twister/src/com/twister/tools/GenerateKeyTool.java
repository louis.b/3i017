package com.twister.tools;

public class GenerateKeyTool {
	
	private static String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890&�&��@%$��";
    private static int charLength = chars.length();
    public static String generateKey(int length) {
        StringBuilder  pass = new StringBuilder (charLength);
        for (int x = 0; x < length; x++) {
            int i = (int) (Math.random() * charLength);
            pass.append(chars.charAt(i));
        }
        return pass.toString();
    }

}

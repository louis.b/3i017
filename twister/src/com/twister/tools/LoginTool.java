package com.twister.tools;

import java.sql.SQLException;

import com.twister.bd.LoginBD;

public class LoginTool {

	public static boolean keyExists(String key) throws SQLException {
		// TODO Auto-generated method stub
		return LoginBD.getIdByKey(key)!=null;
	}

	public static boolean disconnect(String key) {
		// TODO Auto-generated method stub
		return  LoginBD.logout(key);
		
	}

	public static String getId(String key) throws SQLException {
		// TODO Auto-generated method stub
		return LoginBD.getIdByKey(key);
	}

	public static String login(String id, boolean root) {
		// TODO Auto-generated method stub
		return  LoginBD.login(id,root);
	}

	public static boolean isExpired(String key) {
		// TODO Auto-generated method stub
		return LoginBD.isExpired(key);
	}

	public static void refresh(String key) {
		// TODO Auto-generated method stub
		LoginBD.refresh(key);
	}

	public static boolean logoutId(String id_user) {
		// TODO Auto-generated method stub
		return LoginBD.logoutId(id_user);
		
	}

}

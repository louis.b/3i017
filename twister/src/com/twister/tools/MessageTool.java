package com.twister.tools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.twister.bd.CommentBD;
import com.twister.bd.MessageBD;

public class MessageTool {
	public static JSONObject getMessagesUser(String user_id) throws JSONException{
		return MessageBD.getMessagesId(user_id);
	}
	public static boolean deleteMessage(String message_id) {
		return MessageBD.deleteMessage(message_id);
	}
	public static JSONObject getMessagesByIds(int[] auteurs) throws JSONException {
		// TODO Auto-generated method stub
		return MessageBD.getMessagesByIds(auteurs);
	}
	public static boolean messageExists(String comment) {
		// TODO Auto-generated method stub
		return MessageBD.messageExists(comment);
	}
	public static boolean addMessagetoBD(int user_id,String login,String message) {
		return MessageBD.addMessagetoBD(user_id,login,message);
		
	}
	public static boolean addCommenttoBD(int user_id,String login,String message,String message_id) {
		return CommentBD.addCommenttoBD(user_id,login,message,message_id);
		
	}
	public static JSONObject getAllMessages() throws JSONException {
		return MessageBD.getAllMessages();
		
	}
	public static JSONObject getMessagesByIdsQuery(int[] auteurs, String query) throws JSONException {
		// TODO Auto-generated method stub
		return MessageBD.getMessagesByIdsQuery(auteurs,query);
	}
	public static JSONObject getAllMessagesQuery(String query) throws JSONException {
		// TODO Auto-generated method stub
		return MessageBD.getAllMessagesQuery(query);
	}
}

package com.twister.tools;

import com.twister.bd.*;
public class UserTool {

	public static boolean userExists(String login) {
		// TODO Auto-generated method stub
		return UserBD.existsLogin(login);
	}
	public static boolean userExistsId(String id) {
		// TODO Auto-generated method stub
		return UserBD.exists(id);
	}
	public static boolean checkPasswd(String login, String pass) {
		// TODO Auto-generated method stub
		return UserBD.checkPasswd(login,pass);
	}

	public static boolean isRoot(String login) {
		// TODO Auto-generated method stub
		return UserBD.isRoot(login);
	}
	
	public static String getIdbyLogin(String login) {
		return UserBD.getIdbyLogin(login);
	}
	public static String getLoginByID(String id) {
		return UserBD.getLoginByID(id);
	}

	public static boolean addtoBDUser(String login, String pass, String nom, String prenom){
		// TODO Auto-generated method stub
		return UserBD.addtoBDUser(login, pass, prenom, nom); 
		
	}

}

package com.twister.tools;

public class SessionTool {
	public static boolean checkSession(String key) {
		if(LoginTool.isExpired(key)) {
			LoginTool.disconnect(key);
			return false;
		}
		else {
			LoginTool.refresh(key);
			return true;
		}
	}
}

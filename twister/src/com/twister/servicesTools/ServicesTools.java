package com.twister.servicesTools;

import org.json.JSONObject;

public class ServicesTools {
	public static JSONObject error(String msg, int error_num ) {
		try {
			JSONObject ret = new JSONObject();
			ret.put(msg, error_num);
			return ret;			
		}catch(Exception e) {
			return null;
		}
	}
}

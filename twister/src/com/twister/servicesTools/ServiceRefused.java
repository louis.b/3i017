package com.twister.servicesTools;

import org.json.JSONObject;

public class ServiceRefused {
	
	public static JSONObject serviceRefused (String m, int errorid) {
		try {
			JSONObject response = new JSONObject();
			response.put("Message",m);
			response.put("Stat","KO");
			response.put("Errorid",errorid);
			return response;			
		}catch(Exception e) {}
		return null;
	}
}

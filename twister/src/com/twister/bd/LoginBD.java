package com.twister.bd;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.twister.tools.Database;
import com.twister.tools.GenerateKeyTool;

public class LoginBD {
	private static Connection c ;
	public static String getIdByKey(String key) throws SQLException {
		try {
			System.out.println("key "+key);
			Class.forName("com.mysql.jdbc.Driver");
			c = Database.getMySQLConnection();
			String ret = null;
			String query = "SELECT * FROM session WHERE ukey='"+key+"'";
			Statement state = c.createStatement();
			ResultSet result = state.executeQuery(query); 
			while(result.next()) {
				ret = result.getString("id_user");
			}
			state.close();
			c.close();
			System.out.println(ret);
			return ret;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	public static boolean logout(String key) {
		// TODO Auto-generated method stub
		try {

			c = Database.getMySQLConnection();
//			int user_id = LoginBD.getId(key);
//			if(user_id<0) return false;
			String query = "DELETE FROM session WHERE ukey='"+key+"'";
			System.out.println(query);
			Statement state = c.createStatement();
			int result = state.executeUpdate(query);
			state.close();
			c.close();
			return result>0;
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	public static boolean logoutId(String id) {
		// TODO Auto-generated method stub
		try {

			c = Database.getMySQLConnection();
//			int user_id = LoginBD.getId(key);
//			if(user_id<0) return false;
			String query = "DELETE FROM session WHERE id_user="+id;
			Statement state = c.createStatement();
			int result = state.executeUpdate(query);
			state.close();
			c.close();
			return result>0;
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	public static String login(String id_user,boolean root) {
		try {
			c = Database.getMySQLConnection();
			String key = GenerateKeyTool.generateKey(255);// generer
			String query = "INSERT INTO session(id_user,stamp,is_root,ukey) values ('"+id_user+"',NOW(),"+root+",'"+key+"')";
			Statement state = c.createStatement();
			state.executeUpdate(query);
			return key;
			
		}catch(Exception e){
			return "non "+e.getMessage();
		}
	}
	public static boolean refresh(String key) {
		try {
			c = Database.getMySQLConnection();
			String query = "UPDATE session SET stamp = NOW() WHERE ukey='"+key+"'";
			Statement state = c.createStatement();
			state.executeUpdate(query);
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	public static boolean isExpired(String key) {
		try {
			c = Database.getMySQLConnection();
			String query = "SELECT * from session WHERE ukey='"+key+"' AND stamp+3000 < NOW()+0";
			System.out.println(query);
			Statement state = c.createStatement();
			ResultSet result = state.executeQuery(query);
			boolean reponse = result.first();
			state.close();
			c.close();
			return reponse;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}		
	}
	
}

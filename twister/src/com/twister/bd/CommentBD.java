package com.twister.bd;

import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.bson.types.ObjectId;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.*;
import com.mongodb.util.JSON;
import com.twister.tools.Database;

public class CommentBD {
	public static boolean addCommenttoBD(int user_id,String login,String content,String message_id) {
		try {
			DBCollection comments = Database.getCollection("comments");
			BasicDBObject bdo = new BasicDBObject();
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			bdo.put("message_id", new ObjectId(message_id));
			bdo.put("user_id", user_id);
			bdo.put("login", login);
			bdo.put("date", dateFormat.format(date));
			bdo.put("text", content);
			comments.insert(bdo);
			return true;
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (MongoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	public static boolean messageExists(String comment_id) {
		try {
			DBCollection comments = Database.getCollection("comments");
			BasicDBObject bdo = new BasicDBObject();
			bdo.put("_id", new ObjectId(comment_id) );
			return comments.findOne(bdo) != null;
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (MongoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}	
	public static boolean deleteMessage(String message_id) {
		try {
			DBCollection messages = Database.getCollection("comments");
			BasicDBObject bdo = new BasicDBObject();
			bdo.put("_id", new ObjectId(message_id) );
			messages.remove(bdo);
			return true;
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (MongoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	/*La methode getMessages et renvoi la liste de tout les messages
	 * */
	public static JSONObject getAllMessages() throws JSONException {
		try {
			// acces a la collection messages
			DBCollection messages = Database.getCollection("comments");
			if(messages.findOne() == null) {
				return null;
			}
			//creation du curseur pour parcourrir le resultats
		    DBCursor curs= messages.find().sort(new BasicDBObject("date",-1));
			JSONArray ja_messages = new JSONArray();
			JSONObject json_messages = new JSONObject();
			JSONObject[] json = new JSONObject[curs.count()];
			for(int i = 0 ;i<curs.count();i++) {
				//json[i]=new JSONObject(JSON.serialize(curs.next()));
				ja_messages.put(JSON.serialize(curs.next()));
			}
			json_messages.put("messages", ja_messages);
			return json_messages;
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (MongoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	public static JSONObject getAllMessagesQuery(String query) throws JSONException {
		try {
			// acces a la collection messages
			DBCollection messages = Database.getCollection("comments");
			BasicDBObject bdo1 = new BasicDBObject("$regex",query);
			bdo1.put("$options","i");
			BasicDBObject bdo = new BasicDBObject("content", bdo1);
			System.out.println(bdo);
			if(messages.findOne(bdo) == null) {
				System.out.println("out zd");
				return null;
			}
			//creation du curseur pour parcourrir le resultats
		    DBCursor curs= messages.find(bdo).sort(new BasicDBObject("date",-1));
			JSONArray ja_messages = new JSONArray();
			JSONObject json_messages = new JSONObject();
			JSONObject[] json = new JSONObject[curs.count()];
			for(int i = 0 ;i<curs.count();i++) {
				//json[i]=new JSONObject(JSON.serialize(curs.next()));
				ja_messages.put(JSON.serialize(curs.next()));
			}
			json_messages.put("comments", ja_messages);
			return json_messages;
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (MongoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	/*La methode getMessagesIds prend en arguments la liste des auteurs et renvoi la liste de tout leurs messages
	 * */
	public static JSONObject getMessagesByIds(int[] authors) throws JSONException {
		try {
			// acces a la collection messages
			DBCollection messages = Database.getCollection("comments");
			// creation de la querry in pour avoir les auteurs appartenant au tableau
			BasicDBObject bdo = new BasicDBObject("user_id",new BasicDBObject("$in",authors));
			if(messages.findOne(bdo) == null) {
				return null;
			}
			//creation du curseur pour parcourrir le resultats dans lordre deccroissant
		    DBCursor curs= messages.find(bdo).sort(new BasicDBObject("date",-1));
			JSONObject[] json = new JSONObject[curs.count()];
			JSONObject json_messages = new JSONObject();
			JSONArray ja_messages = new JSONArray();
			json_messages.put("total", curs.count());
			for(int i = 0 ;i<curs.count();i++) {
				ja_messages.put(JSON.serialize(curs.next()));
//				json[i]=new JSONObject(JSON.serialize(curs.next()));
			}
			json_messages.put("messages", ja_messages);
			return json_messages;
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (MongoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	public static JSONObject getMessagesByIdsQuery(int[] authors,String query) throws JSONException {
		try {
			// acces a la collection messages
			DBCollection messages = Database.getCollection("comments");
			// creation de la querry in pour avoir les auteurs appartenant au tableau
			BasicDBObject bdo = new BasicDBObject("user_id",new BasicDBObject("$in",authors));
			BasicDBObject bdo1 = new BasicDBObject("$regex",query);
			bdo1.put("$options","i");
			bdo.put("content", bdo1);
			System.out.println(bdo);
			if(messages.findOne(bdo) == null) {
				return null;
			}
			//creation du curseur pour parcourrir le resultats dans lordre deccroissant
		    DBCursor curs= messages.find(bdo).sort(new BasicDBObject("date",-1));
			JSONObject[] json = new JSONObject[curs.count()];
			JSONObject json_messages = new JSONObject();
			JSONArray ja_messages = new JSONArray();
			json_messages.put("total", curs.count());
			for(int i = 0 ;i<curs.count();i++) {
				ja_messages.put(JSON.serialize(curs.next()));
//				json[i]=new JSONObject(JSON.serialize(curs.next()));
			}
			json_messages.put("messages", ja_messages);
			return json_messages;
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (MongoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	public static JSONObject getMessagesId(String id) throws JSONException {
		try {
			// acces a la collection messages
			DBCollection messages = Database.getCollection("comments");
			// creation de la querry in pour avoir les auteurs appartenant au tableau
			BasicDBObject bdo = new BasicDBObject("user_id",id);
			if(messages.findOne(bdo) == null) {
				return null;
			}
			//creation du curseur pour parcourrir le resultats dans lordre deccroissant
		    DBCursor curs= messages.find(bdo).sort(new BasicDBObject("date",-1));
			JSONObject[] json = new JSONObject[curs.count()];
			JSONObject json_messages = new JSONObject();
			JSONArray ja_messages = new JSONArray();
			json_messages.put("total", curs.count());
			for(int i = 0 ;i<curs.count();i++) {
				ja_messages.put(JSON.serialize(curs.next()));
//				json[i]=new JSONObject(JSON.serialize(curs.next()));
			}
			json_messages.put("messages", ja_messages);
			return json_messages;
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (MongoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}

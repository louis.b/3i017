package com.twister.bd;

public class DBStatic {

	public static String mysql_host = "localhost";
	public static String mysql_db = "twister";
	public static String mysql_username = "root";
	public static String mysql_password = "";
	public static boolean mysql_pooling = false;
	public static String mongo_url = "127.0.0.1";
	public static String mongo_db = "twister";
	
}

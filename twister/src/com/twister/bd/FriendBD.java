package com.twister.bd;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONObject;

import com.twister.tools.Database;

public class FriendBD {
	private static Connection c ;
	
	public static boolean addFriend(String key, String id) {
		try {
			c = Database.getMySQLConnection();
			String user_id = LoginBD.getIdByKey(key);
			String query = "INSERT INTO friend(user_id,friend_id,date) values ("+user_id+","+id+",NOW())";
			Statement state = c.createStatement();
			state.executeUpdate(query);
			return true;
			
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	public static boolean removeFriend(String key, String id_friend) {
		// TODO Auto-generated method stub
		try {

			c = Database.getMySQLConnection();
			String user_id = LoginBD.getIdByKey(key);
			String query = "DELETE FROM friend WHERE (friend_id="+id_friend+" AND user_id="+user_id+") OR (user_id="+id_friend+" AND friend_id="+user_id+")";
			System.out.println(query);
			Statement state = c.createStatement();
			boolean reponse = state.execute(query);
			state.close();
			c.close();
			return reponse;
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("out");

			return false;
		}
	}
	public static int[] getFriendsMeId(String id) {
		try {

			c = Database.getMySQLConnection();
			String query = "SELECT * FROM friend WHERE user_id="+id+" OR friend_id="+id;
			Statement state = c.createStatement();
			ResultSet result = state.executeQuery(query);
			List<Integer> auteurs= new ArrayList<>(result.getFetchSize());
			auteurs.add(Integer.parseInt(id));
			int aj;
			while(result.next()) {
				aj = result.getInt("friend_id");
				if(!auteurs.contains(aj)) auteurs.add(aj);
				aj = result.getInt("user_id");
				if(!auteurs.contains(aj)) auteurs.add(aj);				
				}
			state.close();
			c.close();
			int[] list_auteurs = new int[auteurs.size()];
			for(int i = 0; i<auteurs.size() ;i++) {
				list_auteurs[i] = auteurs.get(i).intValue();
			}
			System.out.println("list_auteurs");
			return list_auteurs;
		}catch(Exception e) {
			System.out.println("Problem ajout deja ami");
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] getFriends(String id) {
		try {

			c = Database.getMySQLConnection();
			String query = "SELECT * FROM friend WHERE user_id="+id+" OR friend_id="+id;
			System.out.println(query);
			Statement state = c.createStatement();
			ResultSet result = state.executeQuery(query);
			List<Integer> auteurs= new ArrayList<>();
			int aj;
			int intid = Integer.parseInt(id);
			while(result.next()) {
				aj = result.getInt("friend_id");
				System.out.println(aj);
				if(!auteurs.contains(aj) && intid != aj) {
					auteurs.add(aj);
				}
				aj = result.getInt("user_id");
				System.out.println(aj);
				if(!auteurs.contains(aj) && intid != aj) {
					auteurs.add(aj);
				}
			}
			
			state.close();
			c.close();
			int[] list_auteurs = new int[auteurs.size()];
			for(int i = 0; i<auteurs.size() ;i++) {
				list_auteurs[i] = auteurs.get(i).intValue();
			}
//				System.out.println(auteurs.size());
			return list_auteurs;
		}catch(Exception e) {
			System.out.println("PK ?");
			e.printStackTrace();
			return null;
		}
	}	
	
	
}

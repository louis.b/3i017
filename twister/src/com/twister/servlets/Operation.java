package com.twister.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.twister.services.Operation_Service;

/**
 * Servlet implementation class Operation
 */
public class Operation extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Operation() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String sortie = "";
		try {
			String op =request.getParameter("op");
			double a =Integer.parseInt(request.getParameter("a"));
			double b =Integer.parseInt(request.getParameter("b"));
			sortie += Operation_Service.calcul(a, b, op);

		}catch(Exception e) {
		}
		
	 	response.setContentType( " text / plain " );
		PrintWriter out = response.getWriter ();
		out.println(sortie);
		}

}

package com.twister.servlets.user;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

/**
 * Servlet implementation class Login
 */

public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//Recupere les parametres
		String login = request.getParameter("login");
		String pass = request.getParameter("pass");
		System.out.print(login);
		System.out.print(pass);
		JSONObject ret;
		try {
			ret = com.twister.services.user.Login.login(login,pass);
		}
		catch(Exception e) {
			ret = com.twister.servicesTools.ServiceRefused.serviceRefused("Probleme - Login",500);
		}
		PrintWriter out = response.getWriter();
		response.setContentType("text/plain");
		out.println(ret.toString());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		doGet(request,response);
	}
}

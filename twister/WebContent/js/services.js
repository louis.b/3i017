function signin(){
	var name  = $('#name').val();
	var fname = $('#fname').val();
	var login = $('#login').val();
	var pass  = $('#mdp').val();
	console.log(name);
	$.ajax({
		url: 'UserCreation',
		type: 'get',
		dataType: 'json',
		data: "name="+name+"&fname="+fname+"&login="+login+"&pass="+pass,
		success : function (rep) {
			saveSignin(rep);
		}	
	})
}

function saveSignin(rep){
	if (rep.Status == 'KO') {
		alert(rep.error);
	}
	else if (rep.Status == 'OK') {
		alert(rep.id);
		env.myLogin = rep.login;
		env.myId = rep.id;
		env.key = rep.key;
		makeMainPanel();
	}
}

function connect() {
	var inpseudo = $('#login').val();
	var inpass=$('#mdp').val();
	console.log($('#log').val());
	$.ajax({
		url: 'login',
		type: 'post',
		dataType: 'json',
		data: {login : inpseudo,pass : inpass},
		success : function (rep) {
			saveLogin(rep,inpseudo);
		}
	})
}
function saveLogin(rep,pseudo) {
	if (rep.Status == 'KO') {
		alert(rep.error);
	}
	else if (rep.Status == 'OK') {
		env.myLogin = rep.login;
		env.myId = rep.id;
		env.key = rep.key;
		makeMainPanel();
	}
}

function logout(key) {
	$.ajax({
		url: 'logout',
		type: 'post',
		dataType: 'json',
		data: "key="+encodeURIComponent(key),
		success : function (rep) {
			saveLogout(rep);
		}
	})
}

function saveLogout(rep) {
	if (rep.Status == 'KO') {
		alert(rep.error);
	}
	else if (rep.Status == 'OK') {
		env.myLogin = undefined;
		env.myId = undefined;
		env.key = undefined;
		makeMainPanel();
	}
}

//////////////////////////////////////////

function sendMessage() {
	refreshkey();
	console.log("url "+encodeURIComponent(env.key));
	$.ajax({
		url: 'AddMessage',
		type: 'post',
		dataType: 'json',
		data: "key="+encodeURIComponent(env.key)+"&message="+msg_content,
		success : function (rep) {
			sendMessageProcess(rep);
		}
	})}
	
function sendMessageProcess(rep) {
	refreshkey();

	if (rep.Status == 'KO') {
		alert(rep.error);
	}
	else if (rep.Status == 'OK') {
		alert("msg envoyé");
	}
}

function removeMessage(key,message_id) {
	refreshkey();

	$.ajax({
		url: 'RemoveMessage',
		type: 'get',
		dataType: 'json',
		data: "key="+key+"&message="+message_id,
		success : function (rep) {
			removeMessageProcess(rep);
		}
	})}

function removeMessageProcess(rep) {
	if (rep.Status == 'KO') {
		alert(rep.error);
	}
	else if (rep.Status == 'OK') {

	}
}

function removeMessage(idmes){
	refreshkey();

	$.ajax({
		url: 'RemoveMessage',
		type: 'get',
		dataType: 'json',
		data: {key:env.key,message:idmes},
		success : function (rep) {
			removeMessageProcess(rep);
		}
	})	
}
function removeMessageProcess(rep) {
	if (rep.Status == 'KO') {
		alert(rep.error);
	}
	else if (rep.Status == 'OK') {
		alert("Le message a été supprimé");
	}
}

function getMessages(id_toget){
	$.ajax({
		url: 'ListMessage',
		type: 'post',
		dataType: 'json',
		data: {key:env.key,id:id_toget},
		success : function (rep) {
			getMessageProcess(rep);
		}
	})
}
function getMessageProcess(rep) {
	if (rep.Status == 'KO') {
		alert(rep.error);
	}
	else if (rep.Status == 'OK') {
		console.log($('section').find('.box-messages'));
		ms = rep.messages;
		if(ms != null | ms != undefined){			
			ms.forEach(function(elem){
				if(elem!=null){
					var m = JSON.stringify({ message : elem});
					var mes =  (JSON.parse(m,revival)).getHtml();
					$('section').find('.box-messages').append(mes);			
				}
			})
		}
		$('section').find('.nb_msgs').html( $('section').find(".box-messages").children().length );

	}
}
function getAllMessages(nb){
	$.ajax({
		url: 'ListMessage',
		type: 'post',
		dataType: 'json',
		data: {key:env.key,query:"all"},
		success : function (rep) {
			getMessageProcess(rep);
		}
	})
}
function getMessageProcess(rep) {
	if (rep.Status == 'KO') {
		alert(rep.error);
	}
	else if (rep.Status == 'OK') {
		console.log($('section').find('.box-messages'));
		ms = rep.messages;
		if(ms != null | ms != undefined){			
			ms.forEach(function(elem){
				if(elem!=null){
					var m = JSON.stringify({ message : elem});
					var mes =  (JSON.parse(m,revival)).getHtml();
					$('section').find('.box-messages').append(mes);
				}
			})
			$('section').find('.nb_msgs').html( $('section').find(".box-messages").children().length );
		}
	}
}
//////////////////////////////////////////
function search_msgs(){
	refreshkey();
	console.log("null");
	$.ajax({
		url: 'Search',
		type: 'get',
		dataType: 'json',
		data: "key="+encodeURIComponent(env.key)+"&query="+encodeURIComponent($('nav #search-query').val()),
		success : function (rep) {
			searchProcess(rep);
		}
	})
}
function searchProcess(rep) {
	console.log(rep);
	if (rep.Status == 'KO') {
		alert(rep.error);
	}
	else if (rep.Status == 'OK' || rep.Status == 'Ok') {
		console.log("ok")
		$('section').html(general);
		console.log($('section').find('.box-messages'));
		ms = rep.messages;
		if(ms != null | ms != undefined){			
			ms.forEach(function(elem){
				if(elem!=null){
					var m = JSON.stringify({ message : elem});
					var mes =  (JSON.parse(m,revival)).getHtml();
					$('section').find('.box-messages').append(mes);
				}
			})
			$('section').find('.nb_msgs').html( $('section').find(".box-messages").children().length );
		}
	}
}
////////////////////////////////////////////////////////
function addFriend(friend_id) {
	refreshkey();

	$.ajax({
		url: 'add-friend',
		type: 'get',
		dataType: 'json',
		data: "key="+encodeURIComponent(env.key)+"&friend_id="+friend_id,
		success : function (rep) {
			addFriendProcess(rep,friend_id);
		}
	})}

function addFriendProcess(rep,friend_id) {
	if (rep.Status == 'KO') {
		alert(rep.error);
	}
	else if (rep.Status == 'OK') {
		alert('Ami ajouté !');
		env.myFollows.push(friend_id);

	}
}
function removeFriend(friend_id) {
	refreshkey();

	$.ajax({
		url: 'remove-friend',
		type: 'get',
		dataType: 'json',
		data: "key="+encodeURIComponent(env.key)+"&friend_id="+friend_id,
		success : function (rep) {
			removeFriendProcess(rep,friend_id);
		}
	})}

function removeFriendProcess(rep,friend_id) {
	if (rep.Status == 'KO') {
		alert(rep.error);
	}
	else if (rep.Status == 'OK') {
		alert('Ami retiré !');
		var ind = (env.follows).indexOf(friend_id);
		env.myFollows.splice(ind,1);

	}
}
function getFriends(id) {
	$.ajax({
		url: 'get-friends',
		type: 'post',
		dataType: 'json',
		data: "key="+encodeURIComponent(env.key)+"&id="+id,
		success : function (rep) {
			getFriendsProcess(rep,id);
		}
	})}

function getFriendsProcess(rep,id) {
	if (rep.Status == 'KO') {
		alert(rep.error);
		//
		//
	}
	else if (rep.Status == 'OK') {
		console.log(rep.friends);
		$('section').find('.nb_amis').html( (rep.friends).length );
		if(id==env.myId){
			env.myFollows = rep.friends;			
		}else{
			env.follows = rep.friends;
		}
		rempliramis(rep.friends);

	}
}
function rempliramis(list){
	var html = '<img src="css/imgs/thumb.png" class="img-message" id="{{login}}-{{user_id}}"><span>{{login}}</span>';
	var amilog;
	list.forEach(function(ami){
		amilog=recuperLoginAmi(ami);
		$('section').find('.box-friends').append( Mustache.to_html(html,{user_id:ami,login:amilog}));		
	})
	
}
function recuperLoginAmi(id){
	var ret;
	$.ajax({
		async: false,
		url: 'getlogin',
		type: 'post',
		dataType: 'json',
		data: "key="+encodeURIComponent(env.key)+"&id="+id,
		success : ret = function (rep) {
			console.log(rep.login);
			ret = rep.login;
		}
	})
	return ret;
}

//
function refreshkey(){
	$.ajax({
		url: 'refreshkey',
		type: 'post',
		dataType: 'json',
		data: "key="+encodeURIComponent(env.key),
		success : function (rep) {
			refreshkeyProcess(rep);
		}
	})}

function refreshkeyProcess(rep){
	if(rep.Status == 'KO'){
		env.myLogin = undefined;
		env.myId = undefined;
		env.key = undefined;
		makeMainPanel();		
	}
//	if(rep.Status == 'OK'){
//		env.key = rep.key;
//	}
}
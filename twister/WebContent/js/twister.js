function Message(id,login,text,date,comments) {
	this.id=id;
	this.login=login;
	this.text=text;
	this.date=date;
	if (comments == undefined) {
		comments=[];
	}
	this.comments=comments;
	Message.prototype.getHtml=function () {
		return "<div></div>"
	}
}

function Comment(id,auteur,text,date) {
	this.id=id;
	this.auteur=auteur;
	this.text=text;
	this.date=date;
	Comment.prototype.getHtml=function () {
		return "<div></div>"
	}
}

function revival(key,value) {
	if (value.comments !=undefined) {
		var c = new Message(value.id,value.login,value.text,value.date,value.comments);
		return c;
	}
	else if (value.text != undefined) {
		var c = new Comment(value.id,value.auteur,value.text,value.date);
		return c;
	}
	else if(key=="date"){
		var c = new Date(value);
		return c;
	}
}
function revival(key,value) {
	//console.log(key+" "+value);
	try {
	val = JSON.parse(value);
	} catch(e) {
		//console.log(e);
	}
	if(val!=undefined){
		if (val.comments !=undefined) {
			var c = new Message(val._id,val.user_id,val.login,val.content,val.date,JSON.parse(val.comments));
			//console.log("in");
			return c;
		}
		else if (val.text != undefined) {
			var c = new Comment(val.id,val.auteur,val.text,val.date);
			return c;
		}
		
	}
	else if(key=="date"){
		var c = new Date(val);
		return c;
	}
}
function init(virtual) {
	console.log('initialize');
	env = new Object();
	content = "";
	env.noConnection = virtual;
	env.key = undefined;
	env.follows= [];
	env.myId = undefined;
	env.myLogin = undefined;
	env.friends = undefined;
	env.myFriend = undefined;
	env.myFollows = [];
	if (env.noConnection) {
		setVirtualDb();
		console.log('virual on');

	}
	else{
		makeMainPanel();
		console.log('virual off');
	}
}
function setVirtualDb() {
	localdb=[];
	localdb.push(new Message(2,"mike","yoyo",new Date(),undefined));
	follows[2]=[1,2,3];
	env.myId = 4;
	env.myLogin = 'Jake';
	env.follows = follows;
}
function makeMainPanel(fromId,fromLogin) {
	env.fromId= fromId;
	env.fromLogin= fromLogin;
	if (fromId != undefined && fromLogin !=undefined && fromId != -1 && fromLogin !=-1 && fromId != env.myId) {
		refreshkey();
		$('title').html('La page de '+fromLogin);
		//faire un load pour la page
		var temp_pro = friend;
		var log = {myLogin:fromLogin,id :fromId};
		temp_pro =Mustache.to_html(temp_pro,log);
		$('section').html(temp_pro);
		getMessages(fromId);
		getFriends(fromId);
		if ( env.myFollows.includes(parseInt(fromId)) || env.myFollows.includes(fromId)) {
			//load page
			//$(document).rea
			$('section').find('.retirer-ami').css('display', 'block');
			$('section').find('.ajouter-ami').css('display', 'none');
		}
		else{
			$('section').find('.retirer-ami').css('display', 'none');
			$('section').find('.ajouter-ami').css('display', 'block');
		}
	}
	else if (fromId == -1 && fromId == fromLogin){
		refreshkey();

		$('title').html('Acceuil');
		var temp_pro = general;
		$('section').html(temp_pro);
		getAllMessages();
	}
	else if (env.key != undefined) {
		refreshkey();

		$('title').html('Ma page');
//        data = $("section").load("htmls/profile.html",function(){$("h4").filter($("#profile-name")).css("color","red")});
//        console.log(data);
		getFriends(env.myId);
		var temp_pro = profile;
		var log = {myLogin : env.myLogin};
		temp_pro =Mustache.to_html(temp_pro,log);
		$('section').html(temp_pro);
		getMessages(env.myId);
		//

	}
	else{
		$('title').html('Bienvenue sur Twister');
//      $("section").load("htmls/sign-b.html");
		$("section").html(sign);
	}
}


$('section').on('submit','#message-form',function(){
	if($('#msg').val() == ""){
		alert("message vide !");
		return false;
	}
	msg_content=$('#msg').val();
	
})

$(document).on('submit',".inscrire",function () {
    console.log('Inscription de '+this.pseudo.value);
    var m = this.remdp == undefined ? this.mdp: this.remdp;
    var verif = verifie_sign_form(this.pseudo.value,this.mdp.value,m.value);
    console.log(verif);
    return !verif;
});

function loadSign(){
    $(".tmp").load("htmls/sign-b.html",function(){
    	sign = $(this).html();
    	$(".tmp").html("");
    	});
}
function loadProfile(){
    $(".tmp").load("htmls/profile.html",function(){
    	profile = $(this).html();
    	$(".tmp").html("");

    });
}
function loadLogin(){
    $(".tmp").load("htmls/login-b.html",function(){
    	login = $(this).html();
    	$(".tmp").html("");

    });
}
function loadFriend(){
    $(".tmp").load("htmls/friends.html",function(){
    	friend = $(this).html();
    	$(".tmp").html("");

    });
}
function loadGeneral(){
    $(".tmp").load("htmls/general.html",function(){
    	general = $(this).html();
    	$(".tmp").html("");

    });
}


function fileInit(){
	loadProfile();
	loadFriend();
	loadSign();
	loadLogin();
	loadGeneral();

}



$(document).on('click','.ajouter-ami',function(){
	addFriend(($(this).attr('id')).split('-')[1]);
})
$(document).on('click','.retirer-ami',function(){
	removeFriend(($(this).attr('id')).split('-')[1]);
})
$(document).on('click','.acceuil',function(){
	makeMainPanel(-1,-1);
})
$(document).on('click','.monprofil',function(){
	makeMainPanel();
})
$(document).on('click','.deco',function(){
	logout(env.key);
})
$(document).on('click','.sup-btn',function(){
	console.log($(this).parent().attr('id').split('-')[2]);
	removeMessage($(this).parent().attr('id').split('-')[2]);
	//
	//
})
$(document).on('submit','.search-bar',function(){
	console.log(this.query.value);
	
	if(this.query.value == null){
		console.log("null");
		return false;
	}
	else{
		return true;
	}
})

//

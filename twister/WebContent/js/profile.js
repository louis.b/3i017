/*profile='<div id="overlay">\
        </div>\
        <div id="comment-panel" class="panel" style="margin-left: auto;margin-right: auto;">\
            <button class="btn-red sm btn" id="close-overlay">X</button> Fermer     <h4 class="push-right">Commenter</h4>\
            <hr>\
            <div class="comment-list">\
            </div>\
            <form id="comment-form" style="text-align: center;">\
            <fieldset >\
                <textarea maxlength="196" class="text-noresize" rows="10" cols="50" name="comment-entry" style="text-align: left;"></textarea>\
            </fieldset>\
                <button type="submit" class="btn btn-blue med"> Commenter <span class="icon icon-comment"></span></button>\
            </form> \
        </div>\
        <div id="message-panel" class="panel">\
            <button class="btn-red sm btn" id="close-overlay">X</button> Fermer     <h4 class="push-right">Publier</h4>\
            <hr>\
            <form id="message-form" style="text-align: center;">\
            <fieldset >\
                <textarea maxlength="196" class="text-noresize" rows="10" cols="50" name="message-entry" style="text-align: left;"></textarea>\
            </fieldset>\
                <button type="submit" class="btn btn-blue med"> Publier <span class="icon icon-send"></span></button>\
            </form> \
        </div>\
            <div class="box-profile">\
                <div class="profil">\
                    <img src="css/imgs/thumb.png" class="img-profile">\
                    <div class="profile-text">\
                        <h4 id="profile-name">Mon Pseudo</h4>\
                        <p>mon profil<br><span id="in-date">Inscrit le 00/00/0000</span></p>\
                    </div>\
                </div>\
                <hr>\
                <button class="btn btn-blue">Configuration <span class="icon icon-gear"></span></button>\
                <button class="btn btn-red push-right">Deconnexion</button>\
            </div>\
            <div class="box-messages">\
                </div>\
            </div>\
            <div class="box-friends">\
           </div>\
            <div class="box-stats">\
                <h1>Mes Stats</h1>\
                <hr>\
                <h1>100 Messages <span class="icon icon-mail"></h1>\
                <h1>100 Amis <span class="icon icon-friends"></h1>\
                <h1>100 Croiveurs <span class="icon icon-follows"></h1>\
            </div>\
        <div class="box-outils">\
                <button class="btn-blue sm btn" id="btn-rediger">rediger</button>\
        </div>'
*/
	$("section").on("click",".message-text", function(e) {
	  console.log("Event: ", e);
	  console.log("Current Target of Event: ", e.currentTarget);
	  console.log("this: ", $(this).css("height"));
		$(".message-text").css({
	  	'max-height': '100px',
	  	'overflow': 'hidden',
	  	});
		$(this).css({
	  	'max-height': '400px',
	  	'overflow': 'scroll',
	  });
	  console.log("$(this): ", $(this));
	});
	$(document).on('click', '.com-btn', function(event) {
		console.log("$(this): ", $(this));
		$("#overlay").css('display', 'block');
		$("#comment-panel").css('display', 'block');

	});
	$(document).on('click', '#overlay', function(event) {
		$(this).css('display', 'none');
		$('#msg').val("");
		$(".panel").css('display', 'none');
	});
	$(document).on('click', '#close-overlay', function(event) {
		$("#overlay").css('display', 'none');
		$('#msg').val("");
		$(".panel").css('display', 'none');
	});
	$(document).on('click', '#btn-rediger', function(event) {
		$("#overlay").css('display', 'block');
		$("#message-panel").css('display', 'block');
	});
	$(document).on('click','.img-message',function(){
		var panel_id = $(this).attr('id').split('-');
		var panel_login = panel_id[0];
		var panel_userid = panel_id[1];
		makeMainPanel(panel_userid,panel_login);
	})
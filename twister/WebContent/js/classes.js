function Message(msg_id,id,login,text,date,comments) {
	this.id = msg_id.$oid;
	this.user_id=id;
	this.login=login;
	this.text=text;
	this.date=date;
	if (comments == undefined) {
		comments=[];
	}
	this.comments=comments;
	Message.prototype.getHtml=function () {
		var temp = 
		'<div class="message" id="message-{{user_id}}-{{id}}">'+
			'<div class="message-header">'+
				'<img src="css/imgs/thumb.png" class="img-message" id="{{login}}-{{user_id}}">'+
				'<strong>{{login}}</strong>	'+
				'{{date}}'+
			'</div>'+
			'<div class="message-text">'+
				'{{text}}'+
			'</div>'+( this.user_id == env.myId ? '<button class="btn-red sm btn sup-btn" style="width: 100%">Supprimer</button><button class="btn-blue sm btn" style="width: 100%">Retwist</button>' : " ")+
			'<button class="btn-yellow sm btn com-btn" style="width: 100%">Coms ({{comments.length}})</button>'+
		'</div>';
		return html = Mustache.to_html(temp,this);
	}
}

function Comment(id,auteur,text,date) {
	this.id=id;
	this.auteur=auteur;
	this.text=text;
	this.date=date;
	Comment.prototype.getHtml=function () {
		return "<div></div>"
	}
}
function verifie_sign_form(pseudo,pass,repass) {
	console.log(pseudo.length+" "+pass.length);
	var err = false;
	if (pseudo.length == 0) {
		console.log(pseudo.length+" pas bon ");
		error_form_inscrire("Pseudo Obligatoire","pseudo");
		err = true;
	}
	else if (pseudo.length > 20 ){
		error_form_inscrire("Pseudo trop long, 20 chars max","pseudo");
		err = true;
	}
	else {
		success_form_inscrire("pseudo");
		}
	if (pass.length == 0) {
		error_form_inscrire("Mot de passe Obligatoire","mdp");
		err = true;
	}
	else if (pass!=repass) {
		error_form_inscrire("Les 2 mots de passes sont differents","mdp");
		err = true;
	}
	else{
		success_form_inscrire("mdp");
	}
	return err;		

}
function error_form_inscrire(message,champ){
	var champ = '.inscrire-'+champ;
	$(champ).addClass('error');
	$(champ+' .help-block').html(message);
}
function success_form_inscrire(champ){
	var champ = '.inscrire-'+champ;
	$(champ).removeClass('error');
	$(champ).addClass('succes');
	$(champ+' .help-block').html("");

}